import java.util.Scanner;

public class ApplianceStore {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in); 

        Refrigerator[] appliances = new Refrigerator[4]; 

        for(int i = 0; i < 4; i++){
            appliances[i] = new Refrigerator();

            appliances[i].food = scan.nextLine();
            appliances[i].drinks = Integer.parseInt(scan.nextLine());
            appliances[i].ice = Double.parseDouble(scan.nextLine());;
        }

        System.out.println(appliances[3].food);
        System.out.println(appliances[3].drinks);
        System.out.println(appliances[3].ice);

        appliances[0].coolDown();
        appliances[0].freeze();
    }
}

